# List of Files

To use: place the two files related to a single template (*.json and *.tex) in TexStudio's user data folder (e.g. `C:\Users\X\AppData\Roaming\texstudio\templates\user` on Windows).

| Title                                    | Description                                                  |
| ---------------------------------------- | ------------------------------------------------------------ |
| template_Article (Persian)               | A minimal Persian article template using XePersian. Customize the fonts and the rest of the preamble to your suiting. |
| template_Kindle Oasis 2 English Template | A template based on [this solution](https://tex.stackexchange.com/a/16737/145453) by @ipavlic. |

